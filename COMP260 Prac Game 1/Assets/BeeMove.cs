﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {


    private float speed = 4.0f;

    private float turnSpeed = 180.0f;

    private Transform target;

    private Vector2 heading;

    public float minSpeed, maxSpeed;

    public float minTurnSpeed, maxTurnSpeed;



    // Use this for initialization
    void Start () {

        // find a player object to be the target by type
        PlayerMove player =
              (PlayerMove)FindObjectOfType(typeof(PlayerMove));
        target = player.transform;

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
                                 Random.value);


    }

    // Update is called once per frame
    void Update () {
       
        // get the vector from the bee to the target 
        // and normalise it.
        Vector2 direction = target.position - transform.position;
     
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);

    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);

    }

      
}




