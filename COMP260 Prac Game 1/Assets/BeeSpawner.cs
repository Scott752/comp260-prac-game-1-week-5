﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

    // Use this for initialization
    public int nBees = 50;
    public BeeMove beePrefab;
    public float xMin, yMin;
    public float width, height;

    void Start()
    {
        // create bees
        for (int i = 0; i < nBees; i++)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);

            bee.transform.parent = transform;

            bee.gameObject.name = "Bee " + i;

            // move the bee to a random position within 
            // the bounding rectangle
            float x = xMin + Random.value * width;
            float y = yMin + Random.value * height;
            bee.transform.position = new Vector2(x, y);

        }


    }
	// Update is called once per frame
	void Update () {
	
	}

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
