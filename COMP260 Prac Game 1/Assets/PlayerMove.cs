﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{

    // Use this for initialization

    public Vector2 velocity;

    public float maxSpeed = 5.0f;

    private BeeSpawner beeSpawner;

    public float destroyRadius = 1.0f;

    void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButton("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);

        }

            //Get the Input Values for horizontal and vertical
            Vector2 direction;

        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");


        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;

        //scale the velocity by frame duration
        Vector2 move = velocity * Time.deltaTime;

        // move the object
        transform.Translate(move);


    }
}
